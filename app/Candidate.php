<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Candidate extends Model
{
    protected $fillable = ['name','email','age','comment'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }     

    public static function candidatesnotfit(){
        $notfits = DB::table('candidates')->where('status_id',2)->orwhere('status_id',4)->pluck('id');
        return self::find($notfits)->all(); 
    }

}
