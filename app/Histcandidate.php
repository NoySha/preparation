<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Histcandidate extends Model
{
    protected $fillable = ['name','email','age'];
}
  
