<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Department;
use App\Histcandidate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;


// full name is "App\Http\Controllers\CandidatesController"; 
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {   
        /* משיכת כל המועמדים מטבלת קנדידטס*/
        $allcandidates = Candidate::all(); 
        $candidates = $allcandidates->where('status_id',"!=",4);
        $users = User::all();
        $statuses = Status::all();        
        return view('candidates.index', compact('candidates','users', 'statuses'));
    }

    public function sortages()
    {   
        $candidates = Candidate::orderBy('age')->get();
        $users = User::all();
        $statuses = Status::all();        
        return view('candidates.index', compact('candidates','users', 'statuses'));
    }



    public function notfit()
    {   
        $allcandidates = Candidate::all(); 
        $candidates = Candidate::candidatesnotfit();
        $statuses = Status::all();        
        $users = User::all();
        return view('candidates.index', compact('candidates','users', 'statuses'));
    }



    public function myCandidates()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $candidates = $user->candidates;
        //$candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();        
        return view('candidates.index', compact('candidates','users', 'statuses'));
    }

    public function history()
    {        
        $histcandidates = Histcandidate::orderBy('name', 'desc')->get();
        return view('candidates.history', compact('histcandidates'));
    }





    public function changeUser($cid, $uid = null){
        Gate::authorize('assign-user');
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save(); 
        return back();
        //return redirect('candidates');
    }



    

    public function changeStatus($cid, $sid)
    {
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status', $candidate))
        {
            $from = $candidate->status->id;
            if(!Status::allowed($from,$sid)) return redirect('candidates');        
            $candidate->status_id = $sid;
            $candidate->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user');
        }
        return back();
        //return redirect('candidates');
    }          

    public function addcomment(Request $request, $id)
    {

       $candidate = Candidate::findOrFail($id);
       if(Gate::allows('change-status', $candidate)){
       $candidate->update($request->all());
       return redirect('candidates');  
    }else{
        Session::flash('notallowed2', 'You are not allowed to set comment becuase you are not the owner of the user');
        return back();

    }


    }


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $histcandidate = new Histcandidate();
        $candidate = new Candidate();
        //$candidate->name = $request->name; 
        //$candidate->email = $request->email;
        $hist = $histcandidate->create($request->all());
        $hist->save();
        $can = $candidate->create($request->all());
        $can->status_id = 1;
        $can->save();
        return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $statuses = Status::all();        
        $candidate = Candidate::findOrFail($id); 
        return view('candidates.show', compact('candidate','statuses'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::findOrFail($id);
        return view('candidates.edit', compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $candidate = Candidate::findOrFail($id);
       $candidate->update($request->all());
       return redirect('candidates');  
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('assign-user');
        $candidate = Candidate::findOrFail($id);
        $candidate->delete(); 
        return redirect('candidates'); 
    }
}
