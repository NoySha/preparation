@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
<h1>List of users</h1>

<table class = "table table-dark">
    <tr>
        <th>user</th><th>department</th><th>user_add_id</th>
    </tr>
    @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->department->name}}</td>
            <td>{{$user->user_add_id}}</td>

            <td>
                <a href = "{{route('user.edit',$user->id)}}">Edit</a>
            </td> 
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>                                                               
        </tr>
    @endforeach
</table>
@endsection

