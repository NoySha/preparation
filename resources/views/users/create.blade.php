@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Add User</h1>
        <form method = "post" action = "{{action('UserController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">User name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "email">User email</label>
            <input type = "text" class="form-control" name = "email">
        </div> 
        <div class="form-group">
            <label for = "password">User password</label>
            <input type = "int" class="form-control" name = "password">
        </div> 
        
        <div class="form-group row">
            <label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
             <div class="col-md-6">
                <select class="form-control" name="department_id">                                                                         
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}"> 
                            {{ $department->name }} 
                         </option>
                    @endforeach    
                    
                </select>
            </div>
        <div>
            <input type = "submit" name = "submit" value = "Create User">
        </div>                       
        </form>    
@endsection
