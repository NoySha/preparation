@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
<h1>{{$candidate->name}}</h1>

<ul class="list-group">
  <li class="list-group-item">email: {{$candidate->email}}</li>
  <li class="list-group-item">age: {{$candidate->age}}</li>
  <li class="list-group-item">owner: {{$candidate->owner->name}}</li>
  <li class="list-group-item"> status:
        
  
                <div class="dropdown">
                    @if (null != App\Status::next($candidate->status_id))    
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($candidate->status_id))            
                           {{$candidate->status->name}}
                        @else
                            Define status
                        @endif
                    </button>
                    @else 
                    {{$candidate->status->name}}
                    @endif
                                                   
                    @if (App\Status::next($candidate->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($candidate->status_id) as $status)
                         <a class="dropdown-item">{{$status->name}}</a>
                         <a href = "{{route('candidates.changestatus', [$candidate->id,$status->id])}}">Details</a>
                        @endforeach                               
                    </div>
                    @endif
                    
                </div>            
                               
                       <input type = "submit" name = "submit" value = "Update candidate">

        </li>

</ul>


@endsection

