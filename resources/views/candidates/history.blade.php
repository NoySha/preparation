@extends('layouts.app')

@section('title', 'Candidates')

@section('content')

<h1>History</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Age</th><th>Email</th><th>Created</th><th>Updated</th>
    </tr>
    @foreach($histcandidates as $histcandidate)

        <tr>
            <td>{{$histcandidate->id}}</td>
            <td>{{$histcandidate->name}}</td>
            <td>{{$histcandidate->age}}</td>
            <td>{{$histcandidate->email}}</td>
            <td>{{$histcandidate->created_at}}</td>
            <td>{{$histcandidate->updated_at}}</td>
            <td>
  
    @endforeach
</table>
@endsection

